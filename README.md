# CarCar

Team:

Keith - Service
Hasan - Sales

Diagram:
* Hasan - Sales
* Keith - Services

## Design
![Design of CaCar app ]](<Screenshot 2024-05-10 at 7.27.38 PM.png>)

How to run this app :

URLs and Ports:
sales_rest/api_urls.py:
    urlpatterns = [
        path("salespeople/", api_salespersons, name="get_salesperson"),
        path("salespeople/<int:pk>/", api_salesperson, name="api_salesperson"),
        path("customers/", api_customers, name="api_customers"),
        path("customers/<int:pk>/", api_customer, name="api_customer"),
        path("sales/", api_sales, name="api_sales"),
        path("sales/<int:pk>/", api_sale, name="api_sale"),
]
sales_project/urls.py:
    urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include("sales_rest.api_urls")),
]
service_rest/api_urls.py:
    urlpatterns = [
        path("technicians/", api_technicians, name="api_technicians"),
        path("technicians/<int:pk>/", api_technician, name="api_technician"),
        path("appointments/", api_appointments, name="api_appointments"),
        path("appointments/<str:vin>/", api_appointment, name="api_appointment"),
        path("appointments/<str:vin>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
        path("appointments/<str:vin>/finish/", api_finish_appointment, name="api_finish_appointment"),
]
service_project/urls.py:
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('api/', include("service_rest.api_urls")),
]
Ports:
service port: http://localhost:8080
sales port: http://localhost:8090
inventory port: http://localhost:8100
react port: http://localhost:3000



## Sales microservice

The Sales microservice is contained within the ghi > app > src > Sale. Upon opening the file you will then see the contents of the microservice.
This contains the customers file which has the CustomerForm.js with the code to create a customer and the Customers.js, which will show you a list of the created customers.
Additionally, you will see a sales folder which will contain Sales.js that will show you the code to see a list of the sales made and the SalesForm.js which will allow you to see the code for creating a sale. Lastly, you will see the salespeople folder, that will contain SalesHistory.js and code inside of it to see salespeople's history. Also inside of salespeople, is SalesPeople.js which shows you code to see a list of sales people and SalesPersonForm.js, which will show you the code to create a salesperson.

![](<Screen Shot 2024-05-10 at 7.29.34 PM.png>)

<<<<<<< HEAD
How to run this app:
    Create a "projects" directory in your terminal and fork down the clone from Gitlab using "git clone", once it has been cloned form Gitlab, in your terminal run the commands "docker volume create beta-data", once that is done use "docker-compose build" to build your images, once your images are built run "docker-compose up -d" to build your containers and keep your terminal detached. Type in "code ." to open up VSCode and start coding.

URLs and Ports:
sales_rest/api_urls.py:

    urlpatterns = [
        path("salespeople/", api_salespersons, name="get_salesperson"),
        path("salespeople/<int:pk>/", api_salesperson, name="api_salesperson"),
        path("customers/", api_customers, name="api_customers"),
        path("customers/<int:pk>/", api_customer, name="api_customer"),
        path("sales/", api_sales, name="api_sales"),
        path("sales/<int:pk>/", api_sale, name="api_sale"),
]

sales_project/urls.py:

    urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include("sales_rest.api_urls")),
]

service_rest/api_urls.py:

    urlpatterns = [
        path("technicians/", api_technicians, name="api_technicians"),
        path("technicians/<int:pk>/", api_technician, name="api_technician"),
        path("appointments/", api_appointments, name="api_appointments"),
        path("appointments/<str:vin>/", api_appointment, name="api_appointment"),
        path("appointments/<str:vin>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
        path("appointments/<str:vin>/finish/", api_finish_appointment, name="api_finish_appointment"),
]

service_project/urls.py:

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('api/', include("service_rest.api_urls")),
]

Ports:
service port: http://localhost:8080
sales port: http://localhost:8090
inventory port: http://localhost:8100
react port: http://localhost:3000

Service API:
    The service API helps keep track of the history of service appointments, creating and canceling an appointment and checking if a customer is a VIP if an automobile was pruchased at the dealership. The service API is also responsible for keeping track of technicians by adding new technicians and deleting old technicians. In the models.py file you will see Technician which is responsible for keeping track of the Technician information, first and last name and their employee ID, you will also see Appointment which keeps track of the date, time, reason for service, VIN of the vehicle, the customer coming in for service and the technician working on the vehicle.


=======
Explain your models and integration with the inventory
microservice, here.
>>>>>>> sales
