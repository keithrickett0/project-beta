import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import Technicians from "./Services/Technician/Technicians";
import TechnicianForm from "./Services/Technician/TechnicianForm";
import Appointments from "./Services/Appointments/Appointments";
import AppointmentForm from "./Services/Appointments/AppointmentForm";
import AppointmentHistory from "./Services/Appointments/AppointmentHistory";
import Sales from "./Sale/sales/Sales";
import SalesForm from "./Sale/sales/SalesForm";
import SalesHistory from "./Sale/salespeople/SalesHistory";
import Customers from "./Sale/customers/Customers";
import CustomerForm from "./Sale/customers/CustomerForm";
import SalesPersonForm from "./Sale/salespeople/SalesPersonForm";
import SalesPeople from "./Sale/salespeople/SalesPeople";
import ManufacturerForm from "./Inventory/Manufacturers/ManufacturerForm";
import Manufacturers from "./Inventory/Manufacturers/Manufacturers";
import CarModelList from "./Inventory/CarModels/CarModelList";
import CarModelForm from "./Inventory/CarModels/CarModelForm";
import AutomobileList from "./Inventory/Automobiles/AutomobileList";
import AutomobileForm from "./Inventory/Automobiles/AutomobileForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople">
            <Route index element={<SalesPeople />} />
            <Route path="new" element={<SalesPersonForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="/technicians">
            <Route index element={<Technicians />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="/appointments">
            <Route index element={<Appointments />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="/manufacturers">
            <Route index element={<Manufacturers />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="/models">
            <Route index element={<CarModelList />} />
            <Route path="new" element={<CarModelForm />} />
          </Route>
          <Route path="/automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="/customers">
            <Route index element={<Customers />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="/sales">
            <Route index element={<Sales />} />
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<Manufacturers />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<CarModelList />} />
            <Route path="new" element={<CarModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
