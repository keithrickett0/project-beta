import { useState, useEffect } from "react"


function AutomobileList() {
  const [automobiles, setAutomobile] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/')

    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setAutomobile(data.autos)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(autos => {
            return (
              <tr key={autos.vin}>
                <td>{autos.vin}</td>
                <td>{autos.color}</td>
                <td>{autos.year}</td>
                <td>{autos.model.name}</td>
                <td>{autos.model.manufacturer.name}</td>
                <td>{autos.sold}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default AutomobileList





// "autos": [
//   {
//     "href": "/api/automobiles/2HKRL187X2H576769/",
//     "id": 1,
//     "color": "Black",
//     "year": 2023,
//     "vin": "2HKRL187X2H576769",
//     "model": {
//       "href": "/api/models/1/",
//       "id": 1,
//       "name": "PathFinder",
//       "picture_url": "https://1000logos.net/wp-content/uploads/2020/03/Nissan-Logo-2001.jpg",
//       "manufacturer": {
//         "href": "/api/manufacturers/1/",
//         "id": 1,
//         "name": "Nissan"
//       }
//     },
//     "sold": true
//   }
