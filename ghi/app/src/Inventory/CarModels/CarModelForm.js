import React, { useEffect, useState } from "react"


function CarModelForm() {
  const [manufacturers, setManufacturers] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    manfacturer: '',
    picture_url: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault()

    const modelsUrl = 'http://localhost:8100/api/models/'

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      }
    }

    const response = await fetch(modelsUrl, fetchConfig)

    if (response.ok) {
      setFormData({
        name: '',
        manfacturer: '',
        picture_url: ''
      })
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value
    const inputName = e.target.name
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  return (
    <div className='row'>
    <div className='offset-3 col-6'>
      <div className='shadow p-4 mt-4'>
        <h1>Create a New Car Model</h1>
        <form onSubmit={handleSubmit} id='create-technician-form'>
          <div className='form-floating mb-s'>
            <input onChange={handleFormChange} value={formData.name} placeholder='Name' required type='text' name='name' id='name' className='form-control' />
            <label htmlFor='name'>Name</label>
          </div>
          <div className='form-floating mb-s'>
            <input onChange={handleFormChange} value={formData.picture_url} placeholder='Picture Url' required type='text' name='picture' id='picture' className='form-control' />
            <label htmlFor='picture'>Picture</label>
          </div>
          <div className="mb-3">
              <select onChange={handleFormChange} value={formData.manfacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a Manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.href} value={manufacturer.href}>{manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
          <button className='btn btn-primary'>Create</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default CarModelForm
