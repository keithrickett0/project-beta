import { useState, useEffect } from "react"


function CarModelList() {
  const [models, setModels] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/')

    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setModels(data.models)
    }
  }
  useEffect(() => {
    getData()
  }, [])

  return (
    <>
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.image || 'https://via.placeholder.com/150'}
                    alt="Model"
                    style={{ maxWidth: '100px', maxHeight: '100px' }}
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default CarModelList
