import React, { useEffect, useState } from "react"


function ManufacturerForm() {
  const [manufacturers, setManufacturers] = useState([])
  const [formData, setFormData] = useState({
    name: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault()

    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/'

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(manufacturersUrl, fetchConfig)

    if (response.ok) {
      setFormData({
        name: '',
      })
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value
    const inputName = e.target.name
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  return (
    <div className='row'>
    <div className='offset-3 col-6'>
      <div className='shadow p-4 mt-4'>
        <h1>Create a New Manufacturer</h1>
        <form onSubmit={handleSubmit} id='create-technician-form'>
          <div className='form-floating mb-s'>
            <input onChange={handleFormChange} value={formData.name} placeholder='Manufacturer' required type='text' name='name' id='name' className='form-control' />
            <label htmlFor='name'>Name</label>
          </div>
          <button className='btn btn-primary'>Create</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default ManufacturerForm
