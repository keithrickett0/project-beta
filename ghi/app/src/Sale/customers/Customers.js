import { useEffect, useState } from "react";

function Customers() {
    const [customers, setCustomers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers');

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return(
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Numbers</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customers =>{
                        return (
                            <tr key={customers.href}>
                                <td>{ customers.first_name }</td>
                                <td>{ customers.last_name }</td>
                                <td>{ customers.address}</td>
                                <td>{ customers.phone_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default Customers
