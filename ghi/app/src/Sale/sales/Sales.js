import { useEffect, useState } from "react";

function Sales() {
    const [sale, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sale)
            console.log(data)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return(
        <>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sale.map(sales =>{
                        return (
                            <tr key={sales.automobile}>
                                <td>{ sales.salesperson.employee_id }</td>
                                <td>{ sales.salesperson.first_name } { sales.salesperson.last_name }</td>
                                <td>{ sales.customer.first_name } { sales.customer.last_name }</td>
                                <td>{ sales.automobile.import_vin }</td>
                                <td>{ sales.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default Sales
