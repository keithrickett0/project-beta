import React, { useState, useEffect } from "react";

function SalesForm() {
  const [autos, setAutos] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salesperson, setSalesperson] = useState([]);
  const [formData, setFormData] = useState({
    automobile: "",
    salesperson: "",
    customer: "",
    price: "",
  });

  const getAutos = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  };

  const getCustomers = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  const getSalespersons = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesperson(data.salesperson);
    }
  };

  useEffect(() => {
    getAutos();
  }, []);

  useEffect(() => {
    getCustomers();
  }, []);

  useEffect(() => {
    getSalespersons();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "content-type": "application/json",
      },
    };
    const response = await fetch(salesUrl, fetchConfig);
    console.log(response)

    if (response.ok) {
      setFormData({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
      });
    }
  };
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    if (inputName === "price") {
      setFormData({
        ...formData,
        [inputName]: Number(value),
      });
    } else {
      setFormData({
        ...formData,
        [inputName]: value,
      });
    }

  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Sale</h1>
          <form onSubmit={handleSubmit} id="create-sales-form">
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.automobile}
                name="automobile"
                id="automobile"
                className="form-select"
              >
                <option value="">Choose VIN</option>
                {autos.map( auto => {
                  return (
                    <option key={auto.vin} value={auto.vin}>
                      {auto.vin}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.customer}
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose Customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.customer} value={customer.customer}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.salesperson}
                name="salesperson"
                id="salesperson"
                className="form-select"
              >
                <option value="">Choose Sales Person</option>
                {salesperson.map((salespersons) => {
                  return (
                    <option key={salespersons.salesperson} value={salespersons.salesperson}>
                      {salespersons.first_name} {salespersons.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.price}
                placeholder="price"
                required
                type="number"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
