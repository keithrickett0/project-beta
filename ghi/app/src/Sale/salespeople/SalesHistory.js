import React, { useEffect, useState } from "react";

function SalesHistory() {
    const [sale, setSale] = useState([]);
    const [formData, setFormData] = useState({
        salesperson: '',
        customer: '',
        vin: '',
        price: '',
    });
    const [filteredSales, setFilteredSales] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales');

        if (response.ok) {
            const data = await response.json();
            setSale(data.sale);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "content-type": "application/json",
            },
        };
        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: "",
                salesperson: "",
                customer: "",
                price: "",
            });
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
        const filteredData = sale.filter(s => s.salesperson.first_name === value);
        setFilteredSales(filteredData);
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Sales Person History</h1>
                        <div className="mb-3">
                            <select
                                onChange={handleFormChange}
                                value={formData.salesperson}
                                name="salesperson"
                                id="salesperson"
                                className="form-select"
                            >
                                <option value="">Choose Sales Person</option>
                                {sale.map((sales) => (
                                    <option key={sales.salesperson.first_name} value={sales.salesperson.first_name}>
                                        {sales.salesperson.first_name} {sales.salesperson.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSales.map(sales => (
                                <tr key={sales.automobile}>
                                    <td>{sales.salesperson.first_name} {sales.salesperson.last_name}</td>
                                    <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                                    <td>{sales.automobile.import_vin}</td>
                                    <td>{sales.price}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}

export default SalesHistory;
