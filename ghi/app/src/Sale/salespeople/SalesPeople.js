import { useEffect, useState } from "react";

function SalesPeople() {
    const [salespeople, setSalePeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople');

        if (response.ok) {
            const data = await response.json();
            setSalePeople(data.salesperson)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return(
        <>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson =>{
                        return (
                            <tr key={salesperson.href}>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                                <td>{ salesperson.employee_id }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalesPeople
