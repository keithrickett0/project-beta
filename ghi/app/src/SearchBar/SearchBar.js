import React, { useState } from "react"
import "./SearchBar.css"

function SearchBar({ setResults }) {
  const [input, setInput] = useState("")

  const fetchData = (value) => {
    fetch('http://localhost:8080/api/appointments/').then((response) => response.json()).then((json) => {
      const results = json.filter((appointments) => {
        return (
          value && appointments && appointments.vin && appointments.vin.toLowerCase().includes(value)
        )
      })
      setResults(results)
    })
  }

  const handleChange = (value) => {
    setInput(value)
    fetchData(value)
  }

  return (
    <nav class="input-wrapper">
        <input placeholder="Search by VIN ..." value={input} onChange={(e) => handleChange(e.target.value)} />
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </nav>
  )
}

export default SearchBar
