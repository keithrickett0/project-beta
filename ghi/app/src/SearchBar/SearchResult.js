import React from "react";

import "./SearchResult.css"

function SearchResult({result}) {
    return (
        <div className="search-result">
            {result.vin}
        </div>
    )
}

export default SearchResult
