import React from "react";

import "./SearchResultsList.css"
import SearchResult from "./SearchResult";

function SearchResultsList({ results }) {

  return (
    <div className="results-list">
      {
        results.map((result, vin) => {
          return <SearchResult result={result} key={vin} />
        })
      }
    </div>
  )
}

export default SearchResultsList
