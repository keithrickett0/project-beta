import React, { useState, useEffect } from "react"


function AppointmentForm() {
  const [techs, setTechs] = useState([])
  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date_time: '',
    technician: '',
    reason: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setTechs(data.techs)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault()

    const appointmentUrl = 'http://localhost:8080/api/appointments/'

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(appointmentUrl, fetchConfig)

    if (response.ok) {
      setFormData({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: '',
      })
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value
    const inputName = e.target.name
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a New Appointment</h1>
          <form onSubmit={handleSubmit} id='create-technician-form'>
            <div className='form-floating mb-s'>
              <input onChange={handleFormChange} value={formData.vin} placeholder='Automobile VIN' required type='text' name='vin' id='vin' className='form-control' />
              <label htmlFor='vin'>Automobile VIN</label>
            </div>
            <div className='form-floating mb-s'>
              <input onChange={handleFormChange} value={formData.customer} placeholder='Customer' required type='text' name='customer' id='customer' className='form-control' />
              <label htmlFor='customer'>Customer</label>
            </div>
            <div className='form-floating mb-s'>
              <input onChange={handleFormChange} value={formData.date_time} placeholder='Date Time' required type='datetime-local' name='date_time' id='datetime' className='form-control' />
              <label htmlFor='datetime'>Date</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                <option value="">Choose a Technician</option>
                {techs.map(technician => {
                  return (
                    <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                  )
                })}
              </select>
            </div>
            <div className='form-floating mb-s'>
              <input onChange={handleFormChange} value={formData.reason} placeholder='Reason' required type='text' name='reason' id='reason' className='form-control' />
              <label htmlFor='reason'>Reason</label>
            </div>
            <button className='btn btn-primary'>Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm
