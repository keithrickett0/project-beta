import React, { useState, useEffect } from "react";
import SearchBar from "../../SearchBar/SearchBar";
import SearchResultsList from "../../SearchBar/SearchResultsList";

function AppointmentHistory() {
  const [appointments, setAppointments] = useState([]);
  const [results, setResults] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <h1>Service History</h1>
      <div>
        <SearchBar setResults={setResults} />
        <SearchResultsList results={results} />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td> is car sold? </td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td> (canceled, finsished, created) </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentHistory;
