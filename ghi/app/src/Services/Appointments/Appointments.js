import { useState, useEffect } from "react"


function Appointments() {
  const [appointments, setAppointments] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/')

    if (response.ok) {
      const data = await response.json()
      setAppointments(data.appointments)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <>
      <h1>Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td>yes or no</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button type="button" class="btn btn-danger">Cancel</button>
                  <button type="button" class="btn btn-success">Finish</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default Appointments
