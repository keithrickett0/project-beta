from django.db import models
from django.conf import settings

# Create your models here.


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.import_vin}"


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField()

    def __str__(self):
        return f"{self.first_name}{self.last_name}{self.employee_id}"


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=20, null=True)

    def __str__(self):
        return f"{self.first_name}{self.last_name}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
        related_name="automobile",
        null=True)
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE,
        related_name="sales",
        null=True)
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name="purchases",
        null=True
        )
    price = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return f"{self.automobile}{self.salesperson}{self.customer}"
