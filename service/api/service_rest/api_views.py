from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Technician, Appointment

from .encoders import TechnicianDetailEncoder, AppointmentDetailEncoder

import json


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechnicianDetailEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(
                {'error': str(e)},
                status=400
            )


@require_http_methods(["DELETE", "GET"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=pk)
            return JsonResponse(
                tech,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            tech = Technician.objects.get(id=pk)
            tech.delete()
            return JsonResponse(
                tech,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            print(appointment)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Exception as e:
            print(e)
            response = JsonResponse(
                {"message": "Could not create appointment"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, vin):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(vin=vin)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "PUT"])
def api_cancel_appointment(request, vin):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(vin=vin)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.status = True
            appointment.save()
            return JsonResponse(
                {"message": "Appointment canceled successfully"}
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status = 404
            return response


@require_http_methods(["GET", "PUT"])
def api_finish_appointment(request, vin):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(vin=vin)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.status = True
            appointment.save()
            return JsonResponse(
                {"message": "Appointment finished successfully"}
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status = 404
            return response
