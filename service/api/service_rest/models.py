from django.db import models


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.import_vin}"


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.employee_id}"


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200, null=True)
    status = models.BooleanField(null=True)
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)

    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.date_time} {self.reason} {self.status} {self.vin} {self.customer} {self.technician}"
