import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO


def poll():
    while True:
        try:
            response = requests.get("http://inventory-api:8000/api/automobiles/")
            content = response.json()
            for vin in content["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_vin=vin['vin']
                )
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
